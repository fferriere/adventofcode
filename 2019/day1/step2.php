<?php

$masses = file('./input.txt');

$value = 0;
foreach ($masses as $masse) {
    $value += calcFuel(intval($masse));
}

var_dump($value);

function calcFuel(int $masse): int
{
    if ($masse <= 0) {
        return $masse;
    }

    $value = floor(intval($masse) / 3) - 2;

    if ($value <= 0) {
        $value = 0;
    }

    //var_dump($value);
    return $value + calcFuel($value);
}

/*var_dump(14, calcFuel(14));
var_dump(1969, calcFuel(1969));
var_dump(109044, calcFuel(109044));*/
