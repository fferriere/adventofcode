<?php

function readIntcode(string $intcode): string
{
    $values = explode(',', trim($intcode));
    $values = array_map('intval', $values);

    $values[1] = 12;
    $values[2] = 2;

    $size = count($values);
    $position0 = 0;

    do {
        $value = $values[$position0];
        if ($value === 99) {
            return implode(',', $values);
        }

        $position1 = $values[$position0 + 1];
        $position2 = $values[$position0 + 2];
        $position3 = $values[$position0 + 3];

        $op1 = $values[$position1];
        $op2 = $values[$position2];

        if ($value === 1) {
            $values[$position3] = $op1 + $op2;
        } elseif ($value === 2) {
            $values[$position3] = $op1 * $op2;
        }

        $position0 += 4;
    } while ($position0 <= $size);
}

/*$intcode = '1,0,0,0,99';
var_dump($intcode, readIntcode($intcode));

$intcode = '2,3,0,3,99';
var_dump($intcode, readIntcode($intcode));

$intcode = '2,4,4,5,99,0';
var_dump($intcode, readIntcode($intcode));

$intcode = '1,1,1,4,99,5,6,0,99';
var_dump($intcode, readIntcode($intcode));*/

$intcode = file_get_contents('./input.txt');
var_dump(readIntcode($intcode));
