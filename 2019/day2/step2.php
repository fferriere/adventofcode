<?php

function readIntcode(string $intcode, $noun, $verb): string
{
    $values = explode(',', trim($intcode));
    $values = array_map('intval', $values);

    $values[1] = $noun;
    $values[2] = $verb;

    $size = count($values);
    $position0 = 0;

    do {
        $value = $values[$position0];
        if ($value === 99) {
            return $values[0];
        }

        $position1 = $values[$position0 + 1];
        $position2 = $values[$position0 + 2];
        $position3 = $values[$position0 + 3];

        $op1 = $values[$position1];
        $op2 = $values[$position2];

        if ($value === 1) {
            $values[$position3] = $op1 + $op2;
        } elseif ($value === 2) {
            $values[$position3] = $op1 * $op2;
        }

        $position0 += 4;
    } while ($position0 <= $size);
}

/*$intcode = '1,0,0,0,99';
var_dump($intcode, readIntcode($intcode));

$intcode = '2,3,0,3,99';
var_dump($intcode, readIntcode($intcode));

$intcode = '2,4,4,5,99,0';
var_dump($intcode, readIntcode($intcode));

$intcode = '1,1,1,4,99,5,6,0,99';
var_dump($intcode, readIntcode($intcode));*/

$intcode = file_get_contents('./input.txt');

for ($i = 0; $i <= 99; $i++) {
    for ($j = 0; $j <= 99; $j++) {
        $value = readIntcode($intcode, $i, $j);
        var_dump(sprintf('%d %d => %d', $i, $j, $value));
        /*if (19690720 == $value) {
            die($i * 100 + $j);
        }*/
    }
}
