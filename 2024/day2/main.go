package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("Start")
	nbSafeReport := GetFileContent(os.Args[1], IsSafeReportStep2)
	fmt.Println(nbSafeReport)
}

func GetFileContent(filename string, isSafeReport func([]int) bool) int {
	file, err := os.Open(filename)
	defer file.Close()

	if err != nil {
		panic(err)
	}

	r := bufio.NewReader(file)

	nbSafeReport := 0

	for {
		line, _, err := r.ReadLine()
		if err != nil {
			break
		}

		values := strings.Split(string(line), " ")
		report := make([]int, len(values))
		for i, v := range values {
			iVal, err := strconv.Atoi(v)
			if err != nil {
				panic(err)
			}
			report[i] = iVal
		}

		if isSafeReport(report) {
			nbSafeReport++
		}
	}

	return nbSafeReport
}

func IsSafeReport(report []int) bool {
	firstVal := report[0]
	secondVal := report[1]

	increase := false
	if secondVal > firstVal {
		increase = true
	}

	for i := 1; i < len(report); i++ {
		if increase && report[i] <= report[i-1] {
			return false
		}
		if !increase && report[i] >= report[i-1] {
			return false
		}
		if Abs(report[i]-report[i-1]) > 3 {
			return false
		}
	}

	return true
}

func IsSafeReportStep2(report []int) bool {
	isSafe := IsSafeReport(report)
	if isSafe {
		return true
	}

	for i := 0; i < len(report); i++ {
		dolly := make([]int, len(report))
		_ = copy(dolly, report)
		newReport := remove(dolly, i)
		if IsSafeReport(newReport) {
			return true
		}
	}
	return false
}

func remove(slice []int, s int) []int {
	return append(slice[:s], slice[s+1:]...)
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
