package main

import "testing"

func TestIsSafeReport(t *testing.T) {
	type args struct {
		report []int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "increase report",
			args: args{
				report: []int{1, 2, 3, 4, 5},
			},
			want: true,
		},
		{
			name: "decrease report",
			args: args{
				report: []int{5, 4, 3, 2, 1},
			},
			want: true,
		},
		{
			name: "start by increase report but decrease later",
			args: args{
				report: []int{1, 2, 3, 2, 5},
			},
			want: false,
		},
		{
			name: "start by decrease report but increase later",
			args: args{
				report: []int{5, 4, 3, 4, 1},
			},
			want: false,
		},
		{
			name: "increase report but with a step greater than 3",
			args: args{
				report: []int{1, 2, 3, 4, 10},
			},
			want: false,
		},
		{
			name: "decrease report but with a step greater than 3",
			args: args{
				report: []int{10, 4, 3, 2, 1},
			},
			want: false,
		},
		{
			name: "example report 1",
			args: args{
				report: []int{7, 6, 4, 2, 1},
			},
			want: true,
		},
		{
			name: "example report 2",
			args: args{
				report: []int{1, 2, 7, 8, 9},
			},
			want: false,
		},
		{
			name: "example report 3",
			args: args{
				report: []int{9, 7, 6, 2, 1},
			},
			want: false,
		},
		{
			name: "example report 4",
			args: args{
				report: []int{1, 3, 2, 4, 5},
			},
			want: false,
		},
		{
			name: "example report 5",
			args: args{
				report: []int{8, 6, 4, 4, 1},
			},
			want: false,
		},
		{
			name: "example report 6",
			args: args{
				report: []int{1, 3, 6, 7, 9},
			},
			want: true,
		},
		{
			name: "increase report but with an equality",
			args: args{
				report: []int{1, 4, 4, 6, 8},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsSafeReport(tt.args.report); got != tt.want {
				t.Errorf("IsSafeReport() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsSafeReportStep2(t *testing.T) {
	type args struct {
		report []int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "increase report",
			args: args{
				report: []int{1, 2, 3, 4, 5},
			},
			want: true,
		},
		{
			name: "decrease report",
			args: args{
				report: []int{5, 4, 3, 2, 1},
			},
			want: true,
		},
		{
			name: "start by increase report but decrease later",
			args: args{
				report: []int{1, 2, 3, 2, 5},
			},
			want: true,
		},
		{
			name: "start by decrease report but increase later",
			args: args{
				report: []int{5, 4, 3, 4, 1},
			},
			want: true,
		},
		{
			name: "increase report but with a step greater than 3",
			args: args{
				report: []int{1, 2, 3, 4, 10},
			},
			want: true,
		},
		{
			name: "decrease report but with a step greater than 3",
			args: args{
				report: []int{10, 4, 3, 2, 1},
			},
			want: true,
		},
		{
			name: "example report 1",
			args: args{
				report: []int{7, 6, 4, 2, 1},
			},
			want: true,
		},
		{
			name: "example report 2",
			args: args{
				report: []int{1, 2, 7, 8, 9},
			},
			want: false,
		},
		{
			name: "example report 3",
			args: args{
				report: []int{9, 7, 6, 2, 1},
			},
			want: false,
		},
		{
			name: "example report 4",
			args: args{
				report: []int{1, 3, 2, 4, 5},
			},
			want: true,
		},
		{
			name: "example report 5",
			args: args{
				report: []int{8, 6, 4, 4, 1},
			},
			want: true,
		},
		{
			name: "example report 6",
			args: args{
				report: []int{1, 3, 6, 7, 9},
			},
			want: true,
		},
		{
			name: "increase report but with an equality",
			args: args{
				report: []int{1, 4, 4, 6, 8},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsSafeReportStep2(tt.args.report); got != tt.want {
				t.Errorf("IsSafeReportStep2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAbs(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Abs for positive number",
			args: args{x: 5},
			want: 5,
		},
		{
			name: "Abs for negative number",
			args: args{x: -5},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Abs(tt.args.x); got != tt.want {
				t.Errorf("Abs() = %v, want %v", got, tt.want)
			}
		})
	}
}
