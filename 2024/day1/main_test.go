package main

import (
	"testing"
)

func TestCalculateDistance(t *testing.T) {
	type args struct {
		left  []int
		right []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				left:  []int{1, 5, 3},
				right: []int{3, 2, 4},
			},
			want: 2,
		},
		{
			name: "Example advent",
			args: args{
				left:  []int{3, 4, 2, 1, 3, 3},
				right: []int{4, 3, 5, 3, 9, 3},
			},
			want: 11,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CalculateDistance(tt.args.left, tt.args.right); got != tt.want {
				t.Errorf("CalculateDistance() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAbs(t *testing.T) {
	type args struct {
		x int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Abs for positive number",
			args: args{x: 5},
			want: 5,
		},
		{
			name: "Abs for negative number",
			args: args{x: -5},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Abs(tt.args.x); got != tt.want {
				t.Errorf("Abs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCalculateStep2(t *testing.T) {
	type args struct {
		left  []int
		right []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Test 1",
			args: args{
				left:  []int{1, 5, 3, 3, 4},
				right: []int{3, 2, 4, 3, 1},
			},
			want: 17,
		},
		{
			name: "Example advent",
			args: args{
				left:  []int{3, 4, 2, 1, 3, 3},
				right: []int{4, 3, 5, 3, 9, 3},
			},
			want: 31,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CalculateStep2(tt.args.left, tt.args.right); got != tt.want {
				t.Errorf("CalculateStep2() = %v, want %v", got, tt.want)
			}
		})
	}
}
