package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fmt.Println("Start")
	left, right := GetFileContent(os.Args[1])
	//fmt.Println(CalculateDistance(left, right))
	fmt.Println(CalculateStep2(left, right))
}

func GetFileContent(filename string) ([]int, []int) {
	file, err := os.Open(filename)
	defer file.Close()

	if err != nil {
		panic(err)
	}

	r := bufio.NewReader(file)

	left := make([]int, 0)
	right := make([]int, 0)

	for {
		line, _, err := r.ReadLine()
		if err != nil {
			break
		}
		str := string(line)
		lines := strings.Split(str, "   ")
		lVal, err := strconv.Atoi(lines[0])
		rVal, err := strconv.Atoi(lines[1])
		left = append(left, lVal)
		right = append(right, rVal)
	}

	return left, right
}

func CalculateDistance(left []int, right []int) int {
	result := 0

	sort.Ints(left)
	sort.Ints(right)

	for i := 0; i < len(left); i++ {
		result += Abs(left[i] - right[i])
	}

	return result
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func CalculateStep2(left []int, right []int) int {
	result := 0

	values := TidyRight(right)

	for _, lVal := range left {
		if rVal, ok := values[lVal]; ok {
			result += lVal * rVal
		}
	}

	return result
}

func TidyRight(right []int) map[int]int {
	result := make(map[int]int, len(right))

	for _, val := range right {
		if _, ok := result[val]; !ok {
			result[val] = 1
		} else {
			result[val]++
		}
	}

	return result
}
