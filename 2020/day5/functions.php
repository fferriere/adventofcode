<?php

function calcMinMax(bool $start, int $min, int $max): array
{
    $middle = ($max - $min) / 2;
    if ($start) {
        return [$min, $min + floor($middle)];
    }
    return [$min + ceil($middle), $max];
}

function calcRow(string $letter, int $min, int $max): array
{
    if ('F' === $letter) {
        return calcMinMax(true, $min, $max);
    }
    if ('B' === $letter) {
        return calcMinMax(false, $min, $max);
    }
    throw new \InvalidArgumentException(sprintf('Bad letter : %s', $letter));
}

function calcColumn(string $letter, int $min, int $max): array
{
    if ('L' === $letter) {
        return calcMinMax(true, $min, $max);
    }
    if ('R' === $letter) {
        return calcMinMax(false, $min, $max);
    }
    throw new \InvalidArgumentException(sprintf('Bad letter : %s', $letter));
}

function calcFullRow(string $letters): int
{
    $tab = str_split($letters);
    $min = 0;
    $max = 127;
    foreach ($tab as $letter) {
        list($min, $max) = calcRow($letter, $min, $max);
    }
    return $min;
}

function calcFullColumn(string $letters): int
{
    $tab = str_split($letters);
    $min = 0;
    $max = 7;
    foreach ($tab as $letter) {
        list($min, $max) = calcColumn($letter, $min, $max);
    }
    return $min;
}

function calcSeat(string $seat): int
{
    $row = calcFullRow(substr($seat, 0, 7));
    $col = calcFullColumn(substr($seat, 7, 3));

    return $row * 8 + $col;
}