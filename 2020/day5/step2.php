<?php

require './functions.php';

$seats = file('./input.txt');

$numSeats = [];
foreach ($seats as $seat) {
    $row = calcFullRow(substr($seat, 0, 7));
    $col = calcFullColumn(substr($seat, 7, 3));

    $tabRow = $numSeats[$row] ?? [];
    $tabRow[$col] = $row * 8 + $col;
    $numSeats[$row] = $tabRow;
}

$separation =  "|";
for ($i = 0; $i < 9; $i++) {
    $separation .= "---|";
}
$separation .= PHP_EOL;
for ($i = 0, $size = count($numSeats); $i < $size; $i++) {
    echo $separation;
    $row = $numSeats[$i] ?? [];

    echo sprintf('|%\'. 3d|', $i);
    for ($j = 0; $j < 8; $j++) {
        $col = $row[$j] ?? false;

        if ($col !== true) {
            echo sprintf('%\'. 3d|', $col);
        }
    }
    echo PHP_EOL;
}
echo $separation;