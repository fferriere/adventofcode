<?php

require './functions.php';

$seats = file('./input.txt');

$value = 0;
foreach ($seats as $seat) {
    $value = max($value, calcSeat($seat));
}

var_dump($value);