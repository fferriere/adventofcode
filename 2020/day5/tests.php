<?php

require './functions.php';

//BFFFBBFRRR: row 70, column 7, seat ID 567.
//FFFBBBFRRR: row 14, column 7, seat ID 119.
//BBFFBBFRLL: row 102, column 4, seat ID 820.

function assertEquals($expected, $value, $message)
{
    if ($expected != $value) {
        throw new \Exception(sprintf('%s : %s !== %s', $message, print_r($expected, true), print_r($value, true)));
    }
}

assertEquals([0, 63], calcRow('F', 0, 127), 'calcRow F 0 127');
assertEquals([64, 127], calcRow('B', 0, 127), 'calcRow B 0 127');
assertEquals([0, 31], calcRow('F', 0, 63), 'calcRow F 0 63');
assertEquals([32, 63], calcRow('B', 0, 63), 'calcRow B 0 63');
assertEquals([64, 95], calcRow('F', 64, 127), 'calcRow F 64 127');
assertEquals([96, 127], calcRow('B', 64, 127), 'calcRow B 64 127');
assertEquals([32, 32], calcRow('F', 32, 33), 'calcRow F 32 33');

assertEquals([0, 3], calcColumn('L', 0, 7), 'calcColumn L 0 7');
assertEquals([4, 7], calcColumn('R', 0, 7), 'calcColumn R 0 7');
assertEquals([6, 7], calcColumn('R', 4, 7), 'calcColumn R 4 7');
assertEquals([7, 7], calcColumn('R', 6, 7), 'calcColumn R 6 7');

assertEquals(70, calcFullRow('BFFFBBF'), 'calcFullRow BFFFBBF');
assertEquals(14, calcFullRow('FFFBBBF'), 'calcFullRow FFFBBBF');
assertEquals(102, calcFullRow('BBFFBBF'), 'calcFullRow BBFFBBF');

assertEquals(7, calcFullColumn('RRR'), 'calcFullColumn RRR');
assertEquals(6, calcFullColumn('RRL'), 'calcFullColumn RRL');
assertEquals(5, calcFullColumn('RLR'), 'calcFullColumn RLR');
assertEquals(4, calcFullColumn('RLL'), 'calcFullColumn RLL');
assertEquals(3, calcFullColumn('LRR'), 'calcFullColumn LRR');
assertEquals(2, calcFullColumn('LRL'), 'calcFullColumn LRL');
assertEquals(1, calcFullColumn('LLR'), 'calcFullColumn LLR');
assertEquals(0, calcFullColumn('LLL'), 'calcFullColumn LLL');

assertEquals(567, calcSeat('BFFFBBFRRR'), 'calcSeat BFFFBBFRRR');
assertEquals(119, calcSeat('FFFBBBFRRR'), 'calcSeat FFFBBBFRRR');
assertEquals(820, calcSeat('BBFFBBFRLL'), 'calcSeat BBFFBBFRLL');
