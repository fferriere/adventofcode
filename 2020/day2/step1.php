<?php

$passwords = file('./input.txt');

$results = [];

foreach ($passwords as $line) {
    $result = extractLine($line);
    if (strlen($result) > 0) {
        $results[] = $result;
    }
}

file_put_contents('./result1.txt', print_r($results, true));

function extractLine(string $line): string
{
    $parts = explode(':', $line);
    $password = trim($parts[1]);

    $firstPart = explode(' ', $parts[0]);
    $minMax = explode('-', $firstPart[0]);
    $letter = $firstPart[1];

    $tab = count_chars($password);
    $countLetter = $tab[ord($letter)];

    $work = 0;
    if ($countLetter >= $minMax[0] && $countLetter <= $minMax[1]) {
        $work = 1;
    }
    
    if ($work === 0) {
        return '';
    }
    return sprintf('%d %d %s', $work, $countLetter, $line);
}