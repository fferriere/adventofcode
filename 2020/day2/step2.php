<?php

$passwords = file('./input.txt');

$results = [];

foreach ($passwords as $line) {
    $result = extractLine($line);
    if (strlen($result) > 0) {
        $results[] = $result;
    }
}

file_put_contents('./result2.txt', print_r($results, true));

function extractLine(string $line): string
{
    $parts = explode(':', $line);
    $password = trim($parts[1]);

    $firstPart = explode(' ', $parts[0]);
    $positions = explode('-', $firstPart[0]);
    $letter = $firstPart[1];

    $firstPosition = $positions[0] - 1;
    $secondPosition = $positions[1] - 1;

    $count = 0;

    if ($letter === $password[$firstPosition]) {
        $count++;
    }

    if ($letter === $password[$secondPosition]) {
        $count++;
    }

    if ($count === 1) {
        return $line;
    }

    return '';
}
