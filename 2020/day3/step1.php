<?php

$map = file('./input.txt');

//var_dump(trim($map[0])); die;

$nbCol = strlen(trim($map[0]));
var_dump($nbCol);

$posX = 0;
$nbTrees = 0;
for ($line = 0, $nbLines = count($map); $line < $nbLines; $line++) {
    $square = $map[$line][$posX];
    var_dump(sprintf("%'.03d %'.03d %s", $line, $posX, $square));
    if ('#' === $square) {
        $nbTrees++;
    }
    $posX = ($posX + 3) % $nbCol;
}

var_dump($nbTrees);
