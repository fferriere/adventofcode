<?php

$map = file('./input.txt');

$first = crossMap($map, 1, 1);
$second = crossMap($map, 3, 1);
$third = crossMap($map, 5, 1);
$fourth = crossMap($map, 7, 1);
$fifth = crossMap($map, 1, 2);

var_dump($second);

var_dump($first * $second * $third * $fourth * $fifth);


function crossMap($map, $posXAdd, $nbRowAdd) {
    $nbCol = strlen(trim($map[0]));
    $posX = 0;
    $nbTrees = 0;
    for ($line = 0, $nbLines = count($map); $line < $nbLines; $line += $nbRowAdd) {
        $square = $map[$line][$posX];
        //var_dump(sprintf("%'.03d %'.03d %s", $line, $posX, $square));
        if ('#' === $square) {
            $nbTrees++;
        }
        $posX = ($posX + $posXAdd) % $nbCol;
    }

    return $nbTrees;
}