<?php

$lines = file('./input.txt');

$responses = [];
$i = 0;
foreach ($lines as $line) {
    $line = trim($line);
    if (strlen($line) < 1) {
        $i++;
    }
    $response = $responses[$i] ?? '';
    $response .= $line;
    $responses[$i] = $response;
}

$value = 0;
foreach ($responses as $response) {
    $letters = count_chars($response, 1);
    $value += count($letters);
}

var_dump($value);