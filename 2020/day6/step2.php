<?php

$lines = file('./input.txt');

$responses = [];
$groupNumber = [];
$i = 0;
foreach ($lines as $line) {
    $line = trim($line);
    if (strlen($line) < 1) {
        $i++;
        continue;
    }
    $response = $responses[$i] ?? '';
    $response .= ','.$line;
    $responses[$i] = $response;
    $groupNumber[$i] = ($groupNumber[$i] ?? 0) + 1;
}

$value = 0;
for ($i = 0, $size = count($responses); $i < $size; $i++) {
    $response = $responses[$i];
    var_dump($response);
    $response = str_replace(',', '', $response);
    $letters = count_chars($response, 1);
    print_r($letters);
    $num = $groupNumber[$i];
    foreach ($letters as $char => $number) {
        var_dump($number, $num);
        if ($number === $num) {
            $value++;
        }
    }
}

var_dump($value);