<?php

function findInstruction(string $instruction): array
{
    /*$output = [];
    $regexp = '^(nop|acc|jmp) ([+|-]\d+)$';
    preg_match($regexp, $instruction, $output);
    array_shift($output);
    return $output;*/
    return explode(' ', $instruction);
}

$instructions = file('./input.txt', FILE_IGNORE_NEW_LINES);

function func($instructions, $changement) {
    $acc = 0;

    $pos = 0;

    $passedPosition = [];

    $c = 0;

    do {
        var_dump(sprintf('pos : %d', $pos));
        if (isset($passedPosition[$pos])) {
            break;
        }
        $passedPosition[$pos] = true;
        $str = $instructions[$pos] ?? '';
        if (strlen(trim($str)) < 1) {
            var_dump(sprintf('lui : %d', $acc));
            return $acc;
        }
        list($instruction, $value) = findInstruction($str);
        var_dump(sprintf('i: %s, v: %s', $instruction, $value));
        switch ($instruction) {
            case 'acc':
                $acc += (int) $value;
                $pos++;
                break;
            case 'nop':
                if ($c === $changement) {
                    $pos += (int) $value;
                } else {
                    $pos++;
                }
                $c++;
                break;
            case 'jmp';
                if ($c === $changement) {
                    $pos++;
                } else {
                    $pos += (int) $value;
                }
                $c++;
                break;
        }
    } while (true);

    if (empty($passedPosition)) {
        return $acc;
    }

    return null;
}

$i = 0;
$value = null;
do {
    $value = func($instructions, $i++);

} while($value === null);

var_dump($value);