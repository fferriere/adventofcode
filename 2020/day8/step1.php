<?php

function findInstruction(string $instruction): array
{
    /*$output = [];
    $regexp = '^(nop|acc|jmp) ([+|-]\d+)$';
    preg_match($regexp, $instruction, $output);
    array_shift($output);
    return $output;*/
    return explode(' ', $instruction);
}

$instructions = file('./input.txt', FILE_IGNORE_NEW_LINES);

$acc = 0;

$pos = 0;

$passedPosition = [];

do {
    var_dump($pos);
    if (isset($passedPosition[$pos])) {
        break;
    }
    $passedPosition[$pos] = true;
    list($instruction, $value) = findInstruction($instructions[$pos]);
    var_dump($instruction, $value);
    switch ($instruction) {
        case 'acc':
            $acc += (int) $value;
            var_dump($acc);
        case 'nop':
            $pos++;
            break;
        case 'jmp';
            $pos += (int) $value;
            break;
    }
} while (true);

var_dump($acc);