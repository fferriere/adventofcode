<?php

$reports = file('./input.txt');

$expectedValue = 2020;

for ($i = 0, $size = count($reports); $i < $size - 1; $i++) {
    $initValue = intval($reports[$i]);

    for ($j = $i + 1; $j < $size; $j++) {
        $otherValue = intval($reports[$j]);
        if (($initValue + $otherValue) === $expectedValue) {
            echo sprintf("%d * %d = %d\n", $initValue, $otherValue, $initValue * $otherValue);
        }
    }
}
