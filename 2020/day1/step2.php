<?php

$reports = file('./input.txt');

$expectedValue = 2020;

for ($i = 0, $size = count($reports); $i < $size - 1; $i++) {
    $firstValue = intval($reports[$i]);

    for ($j = $i + 1; $j < $size; $j++) {
        $secondValue = intval($reports[$j]);

        for ($k = $j + 1; $k < $size; $k++) {
            $thirdValue = intval($reports[$k]);

            if (($firstValue + $secondValue + $thirdValue) === $expectedValue) {
                echo sprintf("%d * %d * %d = %d\n", $firstValue, $secondValue, $thirdValue, $firstValue * $secondValue * $thirdValue);
            }
        }
    }
}
