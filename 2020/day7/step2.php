<?php

$rules = file('./input.txt', FILE_IGNORE_NEW_LINES);

$regexp = '/^(\w* \w*) bags contain (.*)\.$/';
$regexpBags = '/(?:(?:(\d) (\w* \w*)) bags?)/';

$completeBags = [];

foreach ($rules as $rule) {
    $output = [];
    preg_match($regexp, $rule, $output);
    $sourceBag = $output[1];
    $contains = $output[2];
    $bags = [];
    preg_match_all($regexpBags, $contains, $bags);

    $sourceBag = $output[1];
    
    $nbBags = count($bags[0]);

    $target = [];
    for ($i = 0; $i < $nbBags; $i++) {
        $target[$bags[2][$i]] = $bags[1][$i];
    }

    $completeBags[$sourceBag] = $target;
}

var_dump($completeBags);

function findNbBag(string $name, array &$allBags): int
{
    $count = 1;

    $subBags = $allBags[$name] ?? [];
    if (empty($subBags)) {
        return $count;
    }

    foreach ($subBags as $bagName => $nbSubBags) {
        $count += $nbSubBags * findNbBag($bagName, $allBags);
    }

    return $count;
}

$search = 'shiny gold';
$count = findNbBag($search, $completeBags) - 1;
var_dump($count);