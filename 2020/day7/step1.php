<?php

$rules = file('./input.txt', FILE_IGNORE_NEW_LINES);

$regexp = '/^(\w* \w*) bags contain (.*)\.$/';
$regexpBags = '/(?:(?:(\d) (\w* \w*)) bags?)/';

$completeBags = [];

foreach ($rules as $rule) {
    $output = [];
    preg_match($regexp, $rule, $output);
    $sourceBag = $output[1];
    $contains = $output[2];
    $bags = [];
    preg_match_all($regexpBags, $contains, $bags);

    $sourceBag = $output[1];
    
    $nbBags = count($bags[0]);

    $target = [];
    for ($i = 0; $i < $nbBags; $i++) {
        $target[$bags[2][$i]] = $bags[1][$i];
    }

    $completeBags[$sourceBag] = $target;
}

function findNbBag(string $name, array &$allBags, array &$parentBags)
{
    foreach ($allBags as $bagName => $subBags) {
        if (array_key_exists($name, $subBags)) {
            $parentBags[$bagName] = true;
            findNbBag($bagName, $allBags, $parentBags);
        }
    }
}

$search = 'shiny gold';
$parentBags = [];
$count = findNbBag($search, $completeBags, $parentBags);
var_dump(count($parentBags));