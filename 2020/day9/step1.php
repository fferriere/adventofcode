<?php

$numbers = file('./input.txt', FILE_IGNORE_NEW_LINES);

$nbPreamble = 25;

function findNumber($numberToFind, $slice, $nbPreamble)
{
    //var_dump($numberToFind, $slice);

    for ($i = 0; $i < ($nbPreamble - 1); $i++) {
        $operandeOne = $slice[$i];
        for ($j = 1; $j < $nbPreamble; $j++) {
            if ($i === $j) {
                continue;
            }
            $operandeTwo = $slice[$j];

            $result = $operandeOne + $operandeTwo;
            
            var_dump(sprintf('%d + %d = %d | %d', $operandeOne, $operandeTwo, $result, $numberToFind));
            if ($result == $numberToFind) {
                return;
            }
        }
    }

    throw new \InvalidArgumentException('', $numberToFind);
}


for ($index = $nbPreamble, $size = count($numbers); $index < $size; $index++) {
    $start = $index - $nbPreamble;
    $slice = array_slice($numbers, $start, $nbPreamble);

    $numberToFind = $numbers[$index];

    try {
        findNumber($numberToFind, $slice, $nbPreamble);
    } catch (\InvalidArgumentException $e) {
        var_dump(sprintf("Lui : %d", $e->getCode()));
        break;
    }
}