<?php

$numbers = file('./input.txt', FILE_IGNORE_NEW_LINES);

$find = 1504371145;

function find($numbers, $numberToFind, $index) {
    $start = $numbers[$index];
    $sum = $start;

    $min = $start;
    $max = $start;

    for ($j = $index + 1, $size = count($numbers); $j < $size; $j++) {
        $num = $numbers[$j];
        $sum += $num;

        $min = min($min, $num);
        $max = max($max, $num);

        if ($sum == $numberToFind) {
            throw new \Exception(sprintf('%d + %d = %d', $min, $max, $min + $max));
        }
    }
}

for ($index = 0, $size = count($numbers); $index < $size; $index++) {
    try {
        find($numbers, $find, $index);
    } catch (\Exception $e) {
        var_dump($e->getMessage());
        break;
    }
}