<?php

$lines = file('./input.txt');
$lines[] = PHP_EOL;

$passports = [];

$string = '';
foreach ($lines as $line) {
    $line = trim($line);
    if (strlen($line) < 1) {
        $passports[] = trim($string);
        $string = '';
        continue;
    }
    $string .= ' ' . $line;
}

$mandatoryKeys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];

$nb = 0;
foreach ($passports as $passport) {
    $tab = explode(' ', $passport);
    $a = [];
    foreach ($tab as $value) {
        $data = explode(':', $value);
        $a[$data[0]] = $data[1];
    }
    $diff = array_diff($mandatoryKeys, array_keys($a));
    if (count($diff) > 0) {
        continue;
    }

    $byr = intval($a['byr']);
    if ($byr < 1920 || $byr > 2002) {
        continue;
    }

    $iyr = intval($a['iyr']);
    if ($iyr < 2010 || $iyr > 2020) {
        continue;
    }

    $eyr = intval($a['eyr']);
    if ($eyr < 2020 || $eyr > 2030) {
        continue;
    }

    $hgt = $a['hgt'];
    if (strpos($hgt, 'cm') !== false) {
        $iHgth = intval($hgt);
        if ($iHgth < 150 || $iHgth > 193) {
            continue;
        }
    } elseif (strpos($hgt, 'in') !== false) {
        $iHgth = intval($hgt);
        if ($iHgth < 59 || $iHgth > 76) {
            continue;
        }
    } else {
        continue;
    }

    $hcl = $a['hcl'];
    if (preg_match('/^#[0-9a-f]{6}$/', $hcl) !== 1) {
        continue;
    }

    if (!in_array($a['ecl'], ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])) {
        continue;
    }

    if (preg_match('/^[0-9]{9}$/', $a['pid']) !== 1) {
        continue;
    }

    $nb++;
}

var_dump($nb);
