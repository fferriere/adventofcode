<?php

$lines = file('./input.txt');
$lines[] = PHP_EOL;

$passports = [];

$string = '';
foreach ($lines as $line) {
    $line = trim($line);
    if (strlen($line) < 1) {
        $passports[] = trim($string);
        $string = '';
        continue;
    }
    $string .= ' ' . $line;
}

$mandatoryKeys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];

$nb = 0;
foreach ($passports as $passport) {
    $tab = explode(' ', $passport);
    $a = [];
    foreach ($tab as $value) {
        $data = explode(':', $value);
        $a[$data[0]] = $data[1];
    }
    $diff = array_diff($mandatoryKeys, array_keys($a));
    if (count($diff) === 0) {
        $nb++;
    } else {
        var_dump($diff);
    }
}

var_dump($nb);
