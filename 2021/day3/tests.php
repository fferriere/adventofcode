<?php

require_once __DIR__.'/functions.php';

function assertSame($expected, $result, $message)
{
    if ($expected !== $result) {
        throw new \Exception(sprintf('%s : %s !== %s', $message, print_r($expected, true), print_r($result, true)));
    } else {
        echo sprintf('%s successful!', $message).PHP_EOL;
    }
}

$inputs = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
assertSame('11111', findMax(['11111', '00000']), 'max first test');
assertSame('11111', findMax(['11111', '00000', '11000']), 'max second test');
assertSame('10111', findMax($inputs), 'max third test');


assertSame('00000', findMin(['11111', '00000']), 'min first test');
assertSame('11000', findMin(['01000', '01100', '11000']), 'min second test');
assertSame('01010', findMin($inputs), 'min third test');
