<?php

//$inputs = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$inputs = file(__DIR__.'/input.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

function findRate(array $inputs)
{
    $nbItems = count($inputs) / 2;
    $resultGamma = array_map(fn (string $element) => (int) $element, str_split(array_shift($inputs)));
    $offset = count($resultGamma);

    foreach ($inputs as $input) {
        $tab = str_split($input);
        for ($i = 0; $i < $offset; $i++) {
            $value = $tab[$i];
            if ($value === "1") {
                $resultGamma[$i]++;
            }
        }
    }

    $bits = array_map(fn ($item) => $item >=$nbItems ? 1 : 0, $resultGamma);

    $gamma = bindec(implode('', $bits));
    $epsilon = bindec(substr(decbin(~$gamma), -1 * $offset));

    return $gamma * $epsilon;
}

var_dump(findRate($inputs));
