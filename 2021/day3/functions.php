<?php

function findMax(array $inputs): string
{
    return findValue($inputs, true);
}

function findMin(array $inputs): string
{
    return findValue($inputs, false);
}

function findValue(array $inputs, bool $isMax): string
{
    $max = strlen($inputs[0]);

    $startStr = '';
    $filteredInputs = $inputs;
    for ($i = 0; $i < $max; $i++) {
        $nbI = 0;

        $bits = array_map(fn ($input) => str_split($input), $filteredInputs);
        $limit = count($filteredInputs) / 2;

        for ($j = 0, $size = count($bits); $j < $size; $j++) {
            $bit = $bits[$j];
            $input = $inputs[$j];

            if ('1' === $bit[$i]) {
                $nbI++;
            }
        }

        if ($isMax) {
            $startStr .= ($nbI >= $limit) ? '1' : '0';
        } else {
            $startStr .= ($nbI >= $limit) ? '0' : '1';
        }

        $filteredInputs = array_values(array_filter($inputs, fn($input) => strpos($input, $startStr) === 0));

        if (count($filteredInputs) === 1) {
            return array_pop($filteredInputs);
        }
    }

    throw new \Exception('Nothing found!');
}
