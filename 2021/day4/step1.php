<?php

require_once __DIR__.'/functions.php';

//$inputs = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$inputs = file(__DIR__.'/input.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

$firstRow = explode(',', array_shift($inputs));

$bingos = [];
$bingoLine = 5;
for ($i = 0, $size = count($inputs); $i < $size; $i++) {
    $bingoIndex = intdiv($i, $bingoLine);
    $bingo = $bingos[$bingoIndex] ?? [];
    $bingo[] = array_values(array_filter(explode(' ', $inputs[$i]), fn($item) => !empty($item) || $item == '0'));
    $bingos[$bingoIndex] = $bingo;
}

$explodeBingos = [];
foreach ($bingos as $bingos) {
    $explodeBingos[] = explodeMatrix($bingos);
}

$lastNumber = null;
$result = array_fill(0, count($explodeBingos), []);
$numberToCheck = [];
foreach ($firstRow as $number) {
    $numberToCheck[] = $number;
    for ($i = 0, $sizeBingo = count($explodeBingos); $i < $sizeBingo; $i++) {
        $bingo = $explodeBingos[$i];
        for ($j = 0, $size = count($bingo); $j < $size; $j++) {
            $row = $bingo[$j];
            for ($k = 0, $sizeRow = count($row); $k < $sizeRow; $k++) {
                $element = $row[$k];
                if ($element === $number) {
                    $res = $result[$i][$j] ?? 0;
                    $res++;
                    $result[$i][$j] = $res;

                    if ($res === 5) {
                        echo 'BINGO'.PHP_EOL.'Row :';
                        echo ' last number :'.$number.PHP_EOL;
                        $lastNumber = $number;
                        break 4;
                    }
                }
            }
        }
    }
}

$toCheck = array_merge(...array_slice($bingo, 0, 5));

var_dump(array_sum(array_diff($toCheck, $numberToCheck)) * $lastNumber);
