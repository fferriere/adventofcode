<?php

function explodeMatrix(array $input): array
{
    $output = $input;

    for ($i = 0, $size = count($input[0]); $i < $size; $i++) {
        $output[] = array_column($input, $i);
    }

    return $output;
}
