<?php

require_once __DIR__.'/functions.php';

//$inputs = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$inputs = file(__DIR__.'/input.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

$firstRow = explode(',', array_shift($inputs));

$bingos = [];
$bingoLine = 5;
for ($i = 0, $size = count($inputs); $i < $size; $i++) {
    $bingoIndex = intdiv($i, $bingoLine);
    $bingo = $bingos[$bingoIndex] ?? [];
    $bingo[] = array_values(array_filter(explode(' ', $inputs[$i]), fn($item) => !empty($item) || $item == '0'));
    $bingos[$bingoIndex] = $bingo;
}

$explodeBingos = [];
foreach ($bingos as $bingos) {
    $explodeBingos[] = explodeMatrix($bingos);
}

$lastNumber = null;
$result = array_fill(0, count($explodeBingos), []);
$numberToCheck = [];

$finishedBoards = [];
$lastNumbers = [];
foreach ($firstRow as $number) {
    $numberToCheck[] = $number;
    for ($i = 0, $sizeBingo = count($explodeBingos); $i < $sizeBingo; $i++) {
        $bingo = $explodeBingos[$i];
        for ($j = 0, $size = count($bingo); $j < $size; $j++) {
            $row = $bingo[$j];
            for ($k = 0, $sizeRow = count($row); $k < $sizeRow; $k++) {
                $element = $row[$k];
                if ($element === $number) {
                    $res = $result[$i][$j] ?? 0;
                    $res++;
                    $result[$i][$j] = $res;

                    if ($res === 5) {
                        $hash = sha1(print_r($bingo, true));
                        if (array_key_exists($hash, $finishedBoards)) {
                            continue;
                        }
                        $finishedBoards[$hash] = $bingo;
                        $lastNumbers[] = $number;
                    }
                }
            }
        }
    }
}

$bingo = array_pop($finishedBoards);
$lastNumber = array_pop($lastNumbers);

$toCheck = array_merge(...array_slice($bingo, 0, 5));

$pos = array_search($lastNumber, $firstRow);
$numberToCheck = array_slice($firstRow, 0, $pos + 1);

var_dump(array_sum(array_diff($toCheck, $numberToCheck)) * $lastNumber);
