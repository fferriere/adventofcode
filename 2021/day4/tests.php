<?php

require_once __DIR__.'/functions.php';

function assertSame($expected, $result, $message)
{
    if ($expected !== $result) {
        throw new \Exception(sprintf('%s : %s !== %s', $message, print_r($expected, true), print_r($result, true)));
    } else {
        echo sprintf('%s successful!', $message).PHP_EOL;
    }
}


assertSame(
    [[1, 2], [3, 4], [1, 3], [2, 4]], 
    explodeMatrix([
        [1, 2],
        [3, 4],
    ]),
    'first explodeMatrix'
);

assertSame(
    [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9]], 
    explodeMatrix([
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]),
    'second explodeMatrix'
);

assertSame(
    [
        [0, 1, 2, 3, 4],
        [5, 6, 7, 8, 9],
        [10, 11, 12, 13, 14],
        [15, 16, 17, 18, 19],
        [20, 21, 22, 23, 24],
        [0, 5, 10, 15, 20],
        [1, 6, 11, 16, 21],
        [2, 7, 12, 17, 22],
        [3, 8, 13, 18, 23],
        [4, 9, 14, 19, 24],
    ], 
    explodeMatrix([
        [0, 1, 2, 3, 4],
        [5, 6, 7, 8, 9],
        [10, 11, 12, 13, 14],
        [15, 16, 17, 18, 19],
        [20, 21, 22, 23, 24],
    ]),
    'third explodeMatrix'
);

//$inputs = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

