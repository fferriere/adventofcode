<?php

$commands = file(__DIR__.'/input.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
//$commands = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

function move(array $commands)
{
    $horizontal = 0;
    $depth = 0;

    foreach ($commands as $command)
    {
        list($direction, $number) = explode(' ', $command);

        switch ($direction) {
            case 'forward':
                $horizontal += $number;
                break;
            case 'down':
                $depth += $number;
                break;
            case 'up':
                $depth -= $number;
                break;
            default:
                break;
        }
    }

    return $horizontal * $depth;
}

var_dump(move($commands));

