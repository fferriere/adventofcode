<?php

$reports = file(__DIR__.'/input.txt', FILE_IGNORE_NEW_LINES);
//$reports = file(__DIR__.'/input_example.txt', FILE_IGNORE_NEW_LINES);

function calculateIncrease(array $reports)
{
    $nbIncrease = 0;
    for ($i = 1, $size = count($reports); $i < $size; $i++) {
        $previousReport = $reports[$i - 1];
        $actualReport = $reports[$i];

        if ($actualReport > $previousReport) {
            $nbIncrease++;
        }
    }

    return $nbIncrease;
}

function calculateIncreaseByWindow(array $reports)
{
    $windowReports = [];
    for ($i = 2, $size = count($reports); $i < $size; $i++) {
        $windowReports[] = $reports[$i - 2] + $reports[$i - 1] + $reports[$i];
    }

    return calculateIncrease($windowReports);
}

var_dump(calculateIncrease($reports));
var_dump(calculateIncreaseByWindow($reports));
